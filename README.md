# RSS2Fastmail

This was an script I wrote to deliver RSS feeds to my Fastmail email box via IMAP. It comes with no guarantees of working however was functional when I stopped using it (~mid-2014). Just because I used it with Fastmail doesn't mean you have to, it should work with any provider who gives IMAP access!

## Configuration

Configuration is a bit awkward as it is a valid python file that can contain functions to filter the feeds, an example config is included.

Per-feed configuration keys:

- 'enabled' (boolean): Determines whether or not the feed should be fetched and delivered.
- 'url' (string): Valid RSS feed URL.
- 'title' (optional, string): Sets the emails FROM header
- 'folder' (optional, string): Mailbox folder to deliver feeds to. If not present feeds will be delivered to the default location.
- 'process' (optional, python function): To further process the feed content you can define a function that takes the feed and returns the processed content.
- 'followlink' (optional, boolean): Fetches the page directed to by the RSS feed and presents it as the content. Can be used when  
    