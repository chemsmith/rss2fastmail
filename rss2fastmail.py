#!/usr/bin/env python3
#
# Python script to send RSS to Fastmail
#
# Copyright (C) 2015  Dylan Smith
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
from __future__ import unicode_literals

__version__ = "0.01"

## import stuffs
import os
import feedparser
import imaplib
import requests

from pytz import timezone
from time import gmtime, mktime, strftime
from datetime import datetime
from email.header import Header
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from hashlib import sha256
from json import dump, load

### import config options from config.py
import config

## initial setup
feedparser.USER_AGENT = "rss2fastmail/" + __version__
feedparser.PREFERRED_XML_PARSERS.remove('drv_libxml2')


def get_date(entry, feed, updated):
    if 'updated_parsed' in entry:
        date = entry.updated_parsed
    elif 'published_parsed' in entry:
        date = entry.published_parsed
    elif 'updated_parsed' in feed.feed:
        date = feed.feed.updated_parsed
    elif 'updated_parsed' in feed:
        date = feed.updated_parsed
    else:
        date = updated
    datetime_unk = datetime.strptime(strftime("%Y-%m-%d %H:%M:%S", date), "%Y-%m-%d %H:%M:%S")
    datetime_utc = datetime_unk.replace(tzinfo=timezone('UTC'))
    return datetime_utc


def get_entry_hash(entry):
    if 'id' in entry:
        entry = entry.id
    elif 'summary' in entry and entry.summary:
        entry = entry.summary
    elif 'title' in entry and entry.title:
        entry = entry.title
    else:
        entry = entry.link
    return sha256(entry.encode('utf-8')).hexdigest()


def get_content_hash(entry):
    if 'summary' in entry and 'title' in entry:
        content = entry.summary + entry.title
    elif 'summary' in entry:
        content = entry.summary
    elif 'title' in entry:
        content = entry.title
    else:
        content = entry.link
    return sha256(content.encode('utf-8')).hexdigest()


def get_flags(flagsresp):
    oldflags = imaplib.ParseFlags(flagsresp)
    newflags = ""
    if oldflags != ():
        for flag in oldflags:
            if flag != '$X-ME-Annot-2':
                newflags = newflags + ' ' + str(flag, 'utf-8')
    newflags = newflags.strip()
    return newflags


def mail(entry, content, headers):
    msg = MIMEMultipart()
    msg['From'] = Header(headers['From'], 'utf-8')
    msg['Date'] = headers['Date']
    msg['Subject'] = Header(headers['Subject'], 'utf-8')
    msg['Message-ID'] = headers['Message-ID']
    msg['RSS-X-UID'] = headers['RSS-X-UID']
    msg['RSS-X-Content'] = headers['RSS-X-Content']
    html = '<html>\n'
    html += '<head><meta http-equiv="Content-Type" content="text/html"><style>img {max-width: 100% !important; height: auto;} body, #body {font-size: 12pt; word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space; font-family: Georgia, Times New Roman, Times, serif;} a:link {color: #0000cc} h1.header a {font-weight: normal; text-decoration: none; color: black;} .summary {font-size: 80%;}</style></head>\n'
    html += '<body style="word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space;">\n'
    html += '<div id="entry">\n<h1 class="header"><a href="%s">%s</a></h1>\n' % (
        entry.link if 'link' in entry else " ", headers['Subject'])
    html += '<div class="summary">%s</div><hr>' % content
    msg.attach(MIMEText(html, 'html', 'utf-8'))
    return msg.as_string()


try:
    cache = load(open(os.path.expanduser("~/.cache/rss2fastmail.json")))
except IOError:
    cache = {}

if config.mailhost['ssl']:
    mailserver = imaplib.IMAP4_SSL(config.mailhost['address'])
else:
    mailserver = imaplib.IMAP4(config.mailhost['address'])
mailserver.login(config.mailhost['username'], config.mailhost['password'])

tz = timezone(config.timezone)
for subscription in config.feeds:
    if 'url' and 'enabled' in subscription and subscription['enabled']:
        currententries = []

        process = subscription['process'] if 'process' in subscription else None
        imapfolder = subscription['folder'] if 'folder' in subscription else config.mailhost['defaultfolder']

        try:
            feedcache = cache[subscription['url']]
        except:
            feedcache = {'entries': {}}

        if 'etag' in feedcache:
            feed = feedparser.parse(subscription['url'], etag=feedcache['etag'])
        elif 'modified' in feedcache:
            feed = feedparser.parse(subscription['url'], modified=feedcache['modified'])
        else:
            feed = feedparser.parse(subscription['url'])

        if 'title' in subscription:
            feedcache['title'] = subscription['title']

        if 'status' in feed:
            if feed.status == 304:
                continue
            if feed.status == 301 or feed.status == 302:
                href = feed.href
                if 'etag' in feedcache:
                    feed = feedparser.parse(href, etag=feedcache['etag'])
                elif 'modified' in feedcache:
                    feed = feedparser.parse(href, modified=feedcache['modified'])
                else:
                    feed = feedparser.parse(href)
            if feed.status == 410:
                continue

        if 'title' not in feedcache:
            try:
                feedcache['title'] = feed.feed.title
            except:
                feedcache['title'] = feed.url

        if 'etag' in feed:
            if feed.etag == (feedcache['etag'] if 'etag' in feedcache else ' '):
                continue
            feedcache['etag'] = feed.etag
        elif 'modified' in feed:
            feedcache['modified'] = feed.modified

        mailings = []

        for entry in reversed(feed.entries):
            updated = False
            uid = get_entry_hash(entry)
            currententries.append(uid)
            contenthash = get_content_hash(entry)

            if uid in feedcache['entries'] and feedcache['entries'][uid]['contenthash'] == contenthash:
                continue
            elif uid in feedcache['entries'] and feedcache['entries'][uid]['contenthash'] != contenthash:
                feedcache['entries'][uid]['contenthash'] = contenthash
                feedcache['entries'][uid]['fetchtime'] = mktime(gmtime())
                updated = True
            else:
                feedcache['entries'][uid] = {}
                feedcache['entries'][uid]['contenthash'] = contenthash
                feedcache['entries'][uid]['fetchtime'] = mktime(gmtime())

            if updated:
                title = "Updated: " + entry.title if 'title' in entry else feed.feed.title
            else:
                title = entry.title if 'title' in entry else feed.feed.title

            title = title.replace("\n", " ").strip()

            when = get_date(entry, feed, gmtime()).astimezone(tz)

            if process:
                if 'followlink' in subscription and subscription['followlink']:
                    try:
                        r = requests.get(entry['link'])
                        r = r.text
                    except:
                        r = False
                    if not r:
                        content = entry.summary if 'summary' in entry else '<a src="%s">Link</a>' % entry.link
                    else:
                        content = process(r)
                else:
                    content = process(entry.summary)
            else:
                content = entry.summary if 'summary' in entry else '<a src="%s">Link</a>' % entry.link

            headers = dict()
            headers['Date'] = when.strftime("%a, %d %b %Y %H:%M:%S %Z%z")
            headers['Subject'] = title
            headers['From'] = feedcache['title']
            headers['RSS-X-UID'] = uid
            headers['Message-ID'] = uid
            headers['RSS-X-Content'] = contenthash

            item = dict()
            item['updated'] = updated
            item['mail'] = mail(entry, content, headers)
            item['date'] = when
            item['uid'] = uid
            item['title'] = title
            mailings.append(item)

        if len(mailings) > 0:
            if mailserver.select(imapfolder)[0] == 'NO':
                mailserver.create(imapfolder)
                mailserver.subscribe(imapfolder)
            mailserver.select(imapfolder)

            try:
                print(feedcache['title'])
            except:
                print(feed.url)

            for item in mailings:
                print("Title: ", item['title'])  # , "\nDate: ", item['date'].strftime("%d-%b-%Y %H:%M:%S %z"))
                if item['updated']:
                    typ, oldmail = mailserver.search(None, '(HEADER Message-ID %s)' % item['uid'])
                    if oldmail[0]:
                        typ, data = mailserver.fetch(oldmail[0], '(FLAGS)')
                        flags = get_flags(data[0])
                        mailserver.store(oldmail[0], '+FLAGS', '\\Deleted')
                        mailserver.append(imapfolder, flags,
                                          "\"" + item['date'].strftime("%d-%b-%Y %H:%M:%S %z") + "\"",
                                          item['mail'].encode('utf-8'))
                else:
                    mailserver.append(imapfolder, '', "\"" + item['date'].strftime("%d-%b-%Y %H:%M:%S %z") + "\"",
                                      item['mail'].encode('utf-8'))
            mailserver.expunge()
        cache[subscription['url']] = feedcache

try:
    mailserver.quit()
except:
    mailserver.logout()

dump(cache, open(os.path.expanduser("~/.cache/rss2fastmail.json"), 'w'), indent=2)
